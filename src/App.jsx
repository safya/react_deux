import { useCallback, useEffect, useState } from 'react'
import reactLogo from './assets/react.svg'
import './App.css'


export default function App() {
  const [count, setCount] = useState(0)


  return (
    <div className="App">
      <h1>Compteur</h1>

      <div className="card">
        <h2>{count}</h2>
        <button onClick={() => setCount(count + 1)}>Ajoute</button>
        <button onClick={() =>{
          setCount(count - 1)
          if(count === 0)
            return setCount(0)
        }}>Retire</button>
        <button onClick={() => setCount(0)}>Reset</button>
      </div>
    </div>
  );
};
